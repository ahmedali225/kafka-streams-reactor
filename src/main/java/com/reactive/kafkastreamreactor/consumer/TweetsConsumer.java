package com.reactive.kafkastreamreactor.consumer;

import com.fasterxml.jackson.databind.JsonNode;
import com.reactive.kafkastreamreactor.config.TweetsProcessor;
import com.reactive.kafkastreamreactor.utils.JsonConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@Component
@EnableBinding(TweetsProcessor.class)
public class TweetsConsumer {

    private static String apply(Message plainMessage) {
        String messageJson = "";
        try {
            messageJson = JsonConverter.toJsonNode((byte[]) plainMessage.getPayload())
                            .map(jsonNode -> jsonNode.get("text").textValue()).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return messageJson;
    }

    @StreamListener
    public void manage(@Input(TweetsProcessor.INPUT) Flux<Message> message) {
        message.log()
                .map(TweetsConsumer::apply)
                .log()
                .doOnError(throwable -> log.error(throwable.getMessage()))
                .subscribe();
    }
}
