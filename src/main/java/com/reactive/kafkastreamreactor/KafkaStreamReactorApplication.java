package com.reactive.kafkastreamreactor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaStreamReactorApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaStreamReactorApplication.class, args);
    }

}
