package com.reactive.kafkastreamreactor.config;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

@Component
public interface TweetsProcessor {

    String INPUT = "twitterInput";

    @Input(INPUT)
    SubscribableChannel twitterInput();
}
