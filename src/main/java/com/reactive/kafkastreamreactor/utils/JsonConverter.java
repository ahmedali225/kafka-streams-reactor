package com.reactive.kafkastreamreactor.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Optional;

public class JsonConverter {

    /**
     * Hiding default constructor.
     */
    private JsonConverter() {
        //nothing
    }

    public static Optional<JsonNode> toJsonNode(byte[] productByte) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return Optional.of(mapper.readTree(productByte));

    }

    public static <T> T toJsonNode(byte[] productByte, Class<T> tClass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(productByte, tClass);
    }
}
